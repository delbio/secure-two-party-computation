// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package cbmc.android.client;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.Option;

import java.io.File;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import Program.CBMCCircuitCommon;
import Program.CBMCProgramClient;
import Program.ProgClient;
import Program.Program;
import Utils.StopWatch;
import YaoGC.Circuit;

public class RunCBMCProgramClient {

	private static final String TAG = "RunCBMCProgramClient";

	private static final BigInteger standardInput = BigInteger.ONE;
	static BigInteger clientInput;

	static Random rnd = new Random();

	private static void printUsage() {
		System.out.println("Usage: java CBMCProgramClient [{-i, --client-input} client-input] [{-s, --server} servername] [{-r, --iteration} r]");
	}

	private static String process_cmdline_args(String[] args) {
		CmdLineParser parser = new CmdLineParser();
		Option optionServerIPname = parser.addStringOption('s', "server");
		Option optionIterCount = parser.addIntegerOption('r', "iteration");
		Option optInputFile = parser.addStringOption("input-file");
		
		try {
			parser.parse(args);
		} catch (CmdLineParser.OptionException e) {
			System.err.println(e.getMessage());
			printUsage();
			System.exit(2);
		}

		ProgClient.serverIPname = (String) parser.getOptionValue(optionServerIPname, new String("localhost"));

		Program.iterCount = ((Integer) parser.getOptionValue(optionIterCount, new Integer(1))).intValue();

		Object valInputFile = parser.getOptionValue(optInputFile);

		if (valInputFile != null) {
			return valInputFile.toString();
		}

		return "inputs.Client.txt";
	}

	private static int readClientInput(int serverLen, String pInputFile) throws Exception {
		clientInput = BigInteger.ZERO;
		int sum = 0;

		File filePartyB = new File(CBMCCircuitCommon.confDir, "output.inputs.partyB.txt");
		HashMap<String, int[]> inputCBMCPositions = CBMCCircuitCommon.readCBMCInputMapping(filePartyB.getAbsolutePath());
		HashMap<String, String> serverInputs = CBMCCircuitCommon.readInputValuesFromFile(pInputFile);

		Iterator<Entry<String, String>> it = serverInputs.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry<String, String> pairs = it.next();
			String key = pairs.getKey();
			BigInteger value = new BigInteger(pairs.getValue());

			if (!inputCBMCPositions.containsKey(key)) {
				System.out.println("*Warning* Key " + key + " not matched");
			} else {

				int[] posLen = inputCBMCPositions.get(key);
				sum += posLen[1];

				if (value.bitLength() > posLen[1]) {
					System.err.println("input value too big / incorrect format");
					System.exit(66);
				}

				BigInteger insert = value.shiftLeft(posLen[0] - 1 - serverLen);
				clientInput = clientInput.or(insert);

				inputCBMCPositions.remove(key);
			}
		}

		if (!inputCBMCPositions.isEmpty()) {
			Iterator<Entry<String, int[]>> it2 = inputCBMCPositions.entrySet().iterator();
			while (it2.hasNext()) {
				Map.Entry<String, int[]> pairs = it2.next();
				String key = pairs.getKey();
				int[] posLen = pairs.getValue();

				System.out.println("*WARNING* Using standard-input for key " + key);

				BigInteger insert = standardInput.shiftLeft(posLen[0] - 1 - serverLen);
				clientInput = clientInput.or(insert);
				sum += posLen[1];
			}
		}

		return sum;
	}


	public static void main(String[] args) throws Exception{

		StopWatch.pointTimeStamp("Starting program");
		String lInputFile = process_cmdline_args(args);
		File filelInputFile = new File(CBMCCircuitCommon.confDir, lInputFile);

		File filePartyA = new File(CBMCCircuitCommon.confDir, "output.inputs.partyA.txt");

		int serverLen = CBMCCircuitCommon.countInputs(filePartyA.getAbsolutePath());
		int clientLen = readClientInput(serverLen, filelInputFile.getAbsolutePath());

		System.out.println("Use Client input: " + clientInput);
		CBMCProgramClient cbmcclient = new CBMCProgramClient(clientInput, serverLen, clientLen);
		cbmcclient.run();
		System.out.println("Garbled Gates Evaluated: " + Circuit.gateCounter);
		System.out.println("Gates in Circuit: " + Circuit.gateCounterConstructor);
		System.out.println("Non-XOR Gates Evaluated: " + Circuit.gateCounterConstructorNonXOR);

	}
}