package cbmc.android.client;

import Utils.commonOutput;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class CBMCClientFragment extends Fragment {

	private static final String TAG = "CBMCClientFragment";

	private Button mStartConnectionButton;
	private EditText mServerIPEdittext;
	private TextView mStatusConnectionTextView;
	private TextView mOutputComputationTextView;

	private SharedPreferences pref;
	private static final String LAST_SERVER_IP_KEY = "last_ip_server";
	
	private String mServerIP;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (BuildConfig.DEBUG) {
			// informazioni sulla memoria allocata per la VM
			String memory = "totalMemory: "+Runtime.getRuntime().totalMemory() +"byte"
					+" maxMemory: "+Runtime.getRuntime().maxMemory() +"byte"
					+" freeMemory: "+Runtime.getRuntime().freeMemory() +"byte"; 

			Log.v("MEMORY STATUS", memory);
			
			pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_cbmc_client, container, false);

		mServerIPEdittext = (EditText) v.findViewById(R.id.server_ip_EditText);
		
		mStatusConnectionTextView = (TextView) v.findViewById(R.id.status_connection_TextView);
		mOutputComputationTextView = (TextView) v.findViewById(R.id.output_computation_TextView);

		mStartConnectionButton = (Button) v.findViewById(R.id.start_connection_Button);
		mStartConnectionButton.setText("Connect");
		mStartConnectionButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mServerIP = mServerIPEdittext.getText().toString();
				new FetchItemsTask().execute(mServerIP);
				mStartConnectionButton.setEnabled(false);
				saveLastIpInPref(mServerIPEdittext.getText().toString());
			}
		});


		return v;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		saveLastIpInPref(mServerIPEdittext.getText().toString());
	}
	
	private void saveLastIpInPref(String currentIP){
		Editor editor = pref.edit();
		editor.putString(LAST_SERVER_IP_KEY, currentIP);
		editor.commit();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		String lastIP = pref.getString(LAST_SERVER_IP_KEY, null);
		
		if (lastIP == null || lastIP.length() == 0 || lastIP.equals("")) return;
		
		mServerIPEdittext.setText(lastIP);			
		
	}

	private class FetchItemsTask extends AsyncTask<String, String, Void>{

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected Void doInBackground(String... params) {

			this.publishProgress("Checking input parameters ...");

			try{

				if (params.length != 1){
					throw new IllegalArgumentException("Necessary only Server IP");
				}

				if (params[0].equals("null") || params[0] == null){
					throw new IllegalArgumentException("Server IP cannot be null");
				}

				if (params[0].length() == 0 || params[0].equals("")){
					throw new IllegalArgumentException("Server IP cannot be empty");
				}

			} catch ( Exception e){
				Log.i(TAG, "error - ",e);
				this.publishProgress("Error: "+e.getMessage());
				return null;
			}

			String[] args = {
					"--server",
					params[0]
			};



			try{

				this.publishProgress("Connection with the server");

				long startTime = System.currentTimeMillis();
				RunCBMCProgramClient.main(args);
				long endTime = System.currentTimeMillis();
				long execTime = (endTime - startTime);

				Log.d(TAG, String.format("**** time %sms", execTime) );
				this.publishProgress(String.format("Execution finished: %sms.", execTime));

			} catch ( Exception e){
				Log.e(TAG, "error - ",e);
				this.publishProgress("Error: "+e.getMessage());
			}



			return null;
		}

		@Override
		protected void onProgressUpdate(String... status) {
			mStatusConnectionTextView.setText(status[0]);
		}

		@Override
		protected void onPostExecute(Void result) {
			mStartConnectionButton.setEnabled(true);
			mOutputComputationTextView.setText("Computation Result: "+commonOutput.computationClientOutput); 
		}

	}
}
