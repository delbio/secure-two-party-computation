// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Test;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.Option;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import Cipher.GCCipher;
import Program.CBMCCircuitCommon;
import Program.CBMCProgramServer;
import Utils.StopWatch;
import YaoGC.Circuit;

class RunCBMCProgramServer {
	private static final BigInteger standardInput = BigInteger.ONE;
	static BigInteger inputServer;

	static Random rnd = new Random();
	
	private static void printUsage() {
		System.out.println("Usage: java CBMCProgramServer [{-n, --bit-length} length]");
	}

	private static String process_cmdline_args(String[] args) {
		CmdLineParser parser = new CmdLineParser();
		Option optInputFile = parser.addStringOption("input-file");

		try {
			parser.parse(args);
		} catch (CmdLineParser.OptionException e) {
			System.err.println(e.getMessage());
			printUsage();
			System.exit(2);
		}
		
		Object valInputFile = parser.getOptionValue(optInputFile);
		
		if (valInputFile != null) {
			return valInputFile.toString();
		}
		
		return "inputs.Server.txt";
	}

	private static int readServerInput(String pInputFile) throws Exception {
		inputServer = BigInteger.ZERO;
		int sum = 0;

		HashMap<String, int[]> inputCBMCPositions = CBMCCircuitCommon.readCBMCInputMapping("output.inputs.partyA.txt");
		HashMap<String, String> serverInputs = CBMCCircuitCommon.readInputValuesFromFile(pInputFile);

		Iterator<Entry<String, String>> it = serverInputs.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry<String, String> pairs = it.next();
			String key = pairs.getKey();
			BigInteger value = new BigInteger(pairs.getValue());

			if (!inputCBMCPositions.containsKey(key)) {
				System.out.println("*Warning* Key " + key + " not matched");
			} else {

				int[] posLen = inputCBMCPositions.get(key);
				sum += posLen[1];

				if (value.bitLength() > posLen[1]) {
					System.err.println("input value to big / incorrect format");
					System.exit(66);
				}

				BigInteger insert = value.shiftLeft(posLen[0] - 1);
				inputServer = inputServer.or(insert);

				inputCBMCPositions.remove(key);
			}
		}

		if (!inputCBMCPositions.isEmpty()) {
			Iterator<Entry<String, int[]>> it2 = inputCBMCPositions.entrySet().iterator();
			while (it2.hasNext()) {
				Map.Entry<String, int[]> pairs = it2.next();
				String key = pairs.getKey();
				int[] posLen = pairs.getValue();

				System.out.println("*WARNING* Using standard-input for key " + key);

				BigInteger insert = standardInput.shiftLeft(posLen[0] - 1);
				inputServer = inputServer.or(insert);
				sum += posLen[1];
			}
		}
		return sum;
	}

	public static void main(String[] args) throws Exception {

		StopWatch.pointTimeStamp("Starting program");
		String lInputFile = process_cmdline_args(args);

		int inputLenServer = readServerInput(lInputFile);
		int inputLenClient = CBMCCircuitCommon.countInputs("output.inputs.partyB.txt");

		System.out.print("Use Server input: ");
		for (int i = inputServer.bitLength(); i > 0; i--) {
			System.out.print((inputServer.testBit(i - 1) ? 1 : 0));
		}
		System.out.print(" = " + inputServer);
		System.out.println(", with a total input length of " + inputLenServer + " bits.");
		StopWatch.taskTimeStamp("Reading Server Inputs from File");
		CBMCProgramServer cbmcserver = new CBMCProgramServer(inputServer, inputLenServer, inputLenClient);
		cbmcserver.run();

		System.out.println("Garbled Gates Evaluated: " + Circuit.gateCounter);
		System.out.println("Gates in Circuit: " + Circuit.gateCounterConstructor);
		System.out.println("Non-XOR Gates Evaluated: " + Circuit.gateCounterConstructorNonXOR);
	}
}