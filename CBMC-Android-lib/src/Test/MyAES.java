package Test;

import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
* This program generates a AES key, retrieves its raw bytes, and
* then reinstantiates a AES key from the key bytes.
* The reinstantiated key is used to initialize a AES cipher for
* encryption and decryption.
*/

public class MyAES {

  /**
  * Turns array of bytes into string
  *
  * @param buf	Array of bytes to convert to hex string
  * @return	Generated hex string
  */
  public static String asHex (byte buf[]) {
   StringBuffer strbuf = new StringBuffer(buf.length * 2);
   int i;

   for (i = 0; i < buf.length; i++) {
    if (((int) buf[i] & 0xff) < 0x10)
	    strbuf.append("0");

    strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
   }

   return strbuf.toString();
  }

  public static void main(String[] args) throws Exception {
		
	  long startTime = System.currentTimeMillis();
	  int aesKeylength = 16;
	  int trialEncryptions = 100000;
	  
//	  String initialMessage = "This is just an example. This is just an example. This is just an example. This is just an example. This is just an example. This is just an example. This is just an example: ";
	  String initialMessage = "This is just an example: ";
	  int anz = (int) Math.round(Math.log10(trialEncryptions)+0.5);
//	  System.out.println(initialMessage.length() + anz + 1);
	  byte[][] storage = new byte[trialEncryptions][initialMessage.length() + anz + 1];
		
	  Cipher cipher;
	  Random rnd = new Random();
	  
		for (int i = 0; i < trialEncryptions; i++) {
//			// Get the KeyGenerator
//			KeyGenerator kgen = KeyGenerator.getInstance("AES");
//			kgen.init(256); // 192 and 256 bits may not be available
			
			byte[] myKey = new byte[aesKeylength];
			for (int j = 0; j < aesKeylength; j++) {
				myKey[j] = (byte) rnd.nextInt(0xff);
			}
			
			// Generate the secret key specs.
			SecretKey skeySpec = new SecretKeySpec(myKey, 0, aesKeylength, "AES");

			// Instantiate the cipher
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

			String message = initialMessage + i;
			byte[] messageBytes = message.getBytes();

			storage[i] = cipher.doFinal(messageBytes);
//			System.out.println("encrypted string: " + asHex(storage[i]));

			cipher.init(Cipher.DECRYPT_MODE, skeySpec);

			// wird spaeter ueberschrieben:
			storage[(i+1)%trialEncryptions] = cipher.doFinal(storage[i]);
//			String originalString = new String(original);
//			System.out.println("Original string: " + originalString + " "
//					+ asHex(original));
		}

		System.out.println("encrypted string: " + asHex(storage[7]));
		
		long gesamt = (System.currentTimeMillis() - startTime);
		System.out.println("Gesamt Zeit: " + gesamt);
		double divided = (double)gesamt/(double)trialEncryptions;
		System.out.println("Zeit pro iteration: " + divided);
		
  }
}
