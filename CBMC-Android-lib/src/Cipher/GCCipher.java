// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Cipher;

import java.math.BigInteger;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class GCCipher {
	
	/*
	 * 1 = SHA1
	 * 2 = AES
	 */
	private static final int GC_CIPHER_MODE = 1;
		
	public static BigInteger encrypt(BigInteger lp0, BigInteger lp1, int k, BigInteger m) {
		
		if (GC_CIPHER_MODE == 1)
			return SHA1Cipher.encrypt(lp0, lp1, k, m);
		else
			return AESCipher.encrypt(lp0, lp1, k, m);
	}
	
	public static BigInteger decrypt(BigInteger lp0, BigInteger lp1, int k, BigInteger c) {
		
		if (GC_CIPHER_MODE == 1)
			return SHA1Cipher.decrypt(lp0, lp1, k, c);
		else
			return AESCipher.decrypt(lp0, lp1, k, c);
	}
	
	public static BigInteger encrypt(BigInteger key, BigInteger msg, int msgLength) {
		
//		if (GC_CIPHER_MODE == 1)
			return SHA1Cipher.encrypt(key, msg, msgLength);
//		else
//			return AESCipher.encrypt(key, msg, msgLength);
	}
	
	public static BigInteger decrypt(BigInteger key, BigInteger cph, int cphLength) {
		
//		if (GC_CIPHER_MODE == 1)
			return SHA1Cipher.decrypt(key, cph, cphLength);
//		else
//			return AESCipher.decrypt(key, cph, cphLength);
	}
	
	public static BigInteger encrypt(int j, BigInteger key, BigInteger msg, int msgLength) {
		
		if (GC_CIPHER_MODE == 1)
			return SHA1Cipher.encrypt(j, key, msg, msgLength);
		else
			return AESCipher.encrypt(j, key, msg, msgLength);
	}
	
	public static BigInteger decrypt(int j, BigInteger key, BigInteger cph, int cphLength) {
		
		if (GC_CIPHER_MODE == 1)
			return SHA1Cipher.decrypt(j, key, cph, cphLength);
		else
			return AESCipher.decrypt(j, key, cph, cphLength);
	}
	
	private static class SHA1Cipher {

		private static final int unitLength = 160; // SHA-1 has 160-bit output.

		private static final BigInteger mask = BigInteger.ONE.shiftLeft(80).subtract(BigInteger.ONE);

		private static MessageDigest sha1 = null;

		static {
			try {
				sha1 = MessageDigest.getInstance("SHA-1");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		public static BigInteger encrypt(BigInteger lp0, BigInteger lp1, int k, BigInteger m) {
			BigInteger ret = getPadding(lp0, lp1, k);
			ret = ret.xor(m);

			return ret;
		}

		public static BigInteger decrypt(BigInteger lp0, BigInteger lp1, int k, BigInteger c) {
			BigInteger ret = getPadding(lp0, lp1, k);
			ret = ret.xor(c);

			return ret;
		}

		// this padding generation function is dedicated for encrypting garbled
		// tables.
		private static BigInteger getPadding(BigInteger lp0, BigInteger lp1, int k) {
			sha1.update(lp0.toByteArray());
			sha1.update(lp1.toByteArray());
			sha1.update(BigInteger.valueOf(k).toByteArray());
			return (new BigInteger(sha1.digest())).and(mask);
		}

		public static BigInteger encrypt(BigInteger key, BigInteger msg, int msgLength) {
			return msg.xor(getPaddingOfLength(key, msgLength));
		}

		public static BigInteger decrypt(BigInteger key, BigInteger cph, int cphLength) {
			return cph.xor(getPaddingOfLength(key, cphLength));
		}

		private static BigInteger getPaddingOfLength(BigInteger key, int padLength) {
			sha1.update(key.toByteArray());
			BigInteger pad = BigInteger.ZERO;
			byte[] tmp = new byte[unitLength / 8];
			for (int i = 0; i < padLength / unitLength; i++) {
				System.arraycopy(sha1.digest(), 0, tmp, 0, unitLength / 8);
				pad = pad.shiftLeft(unitLength).xor(new BigInteger(1, tmp));
				sha1.update(tmp);
			}
			System.arraycopy(sha1.digest(), 0, tmp, 0, unitLength / 8);
			pad = pad.shiftLeft(padLength % unitLength).xor((new BigInteger(1, tmp)).shiftRight(unitLength - (padLength % unitLength)));
			return pad;
		}

		public static BigInteger encrypt(int j, BigInteger key, BigInteger msg, int msgLength) {
			return msg.xor(getPaddingOfLength(j, key, msgLength));
		}

		public static BigInteger decrypt(int j, BigInteger key, BigInteger cph, int cphLength) {
			return cph.xor(getPaddingOfLength(j, key, cphLength));
		}

		private static BigInteger getPaddingOfLength(int j, BigInteger key, int padLength) {
			sha1.update(BigInteger.valueOf(j).toByteArray());
			sha1.update(key.toByteArray());
			BigInteger pad = BigInteger.ZERO;
			byte[] tmp = new byte[unitLength / 8];
			for (int i = 0; i < padLength / unitLength; i++) {
				System.arraycopy(sha1.digest(), 0, tmp, 0, unitLength / 8);
				pad = pad.shiftLeft(unitLength).xor(new BigInteger(1, tmp));
				sha1.update(tmp);
			}
			System.arraycopy(sha1.digest(), 0, tmp, 0, unitLength / 8);
			pad = pad.shiftLeft(padLength % unitLength).xor((new BigInteger(1, tmp)).shiftRight(unitLength - (padLength % unitLength)));
			return pad;
		}
	}
	
	private static class AESCipher {

		// bitlength AES
		private static final int unitLength = 128;

		private static final BigInteger mask = BigInteger.ONE.shiftLeft(80).subtract(BigInteger.ONE);
		private static final byte[] ONES = mask.toByteArray();

		/*
		 * LEN_CIPHER: Laenge der GC keys
		 */
		private static final int LEN_CIPHER = 80;

		private static Cipher cipher = null;
		static {
			try {
				cipher = Cipher.getInstance("AES");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		public static BigInteger encrypt(BigInteger lp0, BigInteger lp1, int k, BigInteger m) {

			BigInteger ret = getPadding(lp0, lp1, k);
			return ret.xor(m);
		}

		public static BigInteger decrypt(BigInteger lp0, BigInteger lp1, int k, BigInteger c) {

			BigInteger ret = getPadding(lp0, lp1, k);
			return ret.xor(c);
		}

		// this padding generation function is dedicated for encrypting garbled
		// tables.
		private static BigInteger getPadding(BigInteger lp0, BigInteger lp1, int k) {

			// shifte alles zurecht
			// setze hoechstes bit, damit java zufrieden ist
			BigInteger v = lp0.or(((lp1.or(BigInteger.valueOf(k).shiftLeft(LEN_CIPHER))).shiftLeft(LEN_CIPHER))).setBit(254);

			SecretKeySpec skeySpec = new SecretKeySpec(v.toByteArray(), "AES");
			try {
				cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
				return (new BigInteger(cipher.doFinal(ONES))).and(mask);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

//		public static BigInteger encrypt(BigInteger key, BigInteger msg, int msgLength) {
//			return msg.xor(getPaddingOfLength(key, msgLength));
//		}
//
//		public static BigInteger decrypt(BigInteger key, BigInteger cph, int cphLength) {
//			return cph.xor(getPaddingOfLength(key, cphLength));
//		}

//		private static BigInteger getPaddingOfLength(BigInteger key, int padLength) {
//
//			SecretKeySpec skeySpec = new SecretKeySpec(key.toByteArray(), "AES");
//			try {
//				cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//				byte[] m = ONES;
//
//				BigInteger pad = BigInteger.ZERO;
//				byte[] tmp = new byte[unitLength / 8];
//				for (int i = 0; i < padLength / unitLength; i++) {
//					System.arraycopy(cipher.doFinal(m), 0, tmp, 0, unitLength / 8);
//					pad = pad.shiftLeft(unitLength).xor(new BigInteger(1, tmp));
//					m = tmp;
//				}
//				System.arraycopy(cipher.doFinal(m), 0, tmp, 0, unitLength / 8);
//				pad = pad.shiftLeft(padLength % unitLength).xor((new BigInteger(1, tmp)).shiftRight(unitLength - (padLength % unitLength)));
//				return pad;
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			return null;
//		}

		public static BigInteger encrypt(int j, BigInteger key, BigInteger msg, int msgLength) {
			return msg.xor(getPaddingOfLength(j, key, msgLength));
		}

		public static BigInteger decrypt(int j, BigInteger key, BigInteger cph, int cphLength) {
			return cph.xor(getPaddingOfLength(j, key, cphLength));
		}

		private static BigInteger getPaddingOfLength(int j, BigInteger key, int padLength) {
			
			byte[] rKey = key.shiftLeft(15).xor(BigInteger.valueOf(j)).setBit(254).toByteArray();
			rKey[31] = (byte)100;
			SecretKeySpec skeySpec = new SecretKeySpec(rKey, "AES");
			try {
				cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
				byte[] m = ONES;

				BigInteger pad = BigInteger.ZERO;
				byte[] tmp = new byte[unitLength / 8];
				for (int i = 0; i < padLength / unitLength; i++) {					
					System.arraycopy(cipher.doFinal(m), 0, tmp, 0, unitLength / 8);
					pad = pad.shiftLeft(unitLength).xor(new BigInteger(1, tmp));
					m = tmp;
				}
				byte[] adjf = cipher.doFinal(m);
				System.arraycopy(adjf, 0, tmp, 0, unitLength / 8);
				pad = pad.shiftLeft(padLength % unitLength).xor((new BigInteger(1, tmp)).shiftRight(unitLength - (padLength % unitLength)));
				return pad;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
	}
}