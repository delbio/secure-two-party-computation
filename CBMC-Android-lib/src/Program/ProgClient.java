// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Program;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;

import android.util.Log;

import Utils.*;
import OT.*;
import YaoGC.*;

public abstract class ProgClient extends Program {

	public static String serverIPname = "localhost";             // server IP name
	private final int    serverPort   = 23456;                   // server port number
	private Socket       sock         = null;                    // Socket object for communicating

	protected int otNumOfPairs;
	protected Receiver rcver;

	private static final String TAG = "ProgClient";

	public void run() throws Exception {
		int number_of_tries = 0;
		boolean stop = false;

		do {
			try {
				create_socket_and_connect();
				stop = true;
			}
			catch (java.net.ConnectException e) {
				System.err.println("Sleeping for 800 ms and then try to reconnect to the server.");
				number_of_tries++;
				Thread.sleep(800);
			}
		}
		while (number_of_tries < 10 && !stop);

		if (stop) {
			super.run();
			number_of_tries = 0;
			stop = false;

			Log.d(TAG,"Connecting to the client for receiving results of the computation");
			do {
				try {

					sock = new java.net.Socket(serverIPname, serverPort+1);          // create socket and connect
					stop = true;
				}
				catch (java.net.SocketException e) {
					System.err.println("Sleeping for 100 ms and then try to reconnect to the server. Attempt: "+number_of_tries+"/10");
					number_of_tries++;
					Thread.sleep(100);
				}
			}
			while (number_of_tries < 10 && !stop);


			InputStream mmInStream = sock.getInputStream();

			byte[] receivedData = new byte[512];
			
			//This is the separetor to split the output and the stream SHIT
			String streamEnd = "<>END";
			//Receiving the output
			int num = mmInStream.read(receivedData); //Leggiamo la credenzale tutta in un colpo
			String computationOutput = new String(receivedData,"US-ASCII");
			String data[] = computationOutput.split(streamEnd);
			Log.d(TAG,"Computation Output: "+ data[0]);
			
			commonOutput.computationClientOutput = data[0];

			//We close everything
			if (mmInStream != null) {
				mmInStream.close();
			}
			if (sock != null) {
				sock.close();
			}

		}
		else {
			System.err.println("Couldn't connect to server!");
		}

		cleanup();
	}

	protected void init() throws Exception {
		System.out.println(Program.iterCount);
		ProgCommon.oos.writeInt(Program.iterCount);
		ProgCommon.oos.flush();

		super.init();
	}

	private void create_socket_and_connect() throws Exception {
		sock = new java.net.Socket(serverIPname, serverPort);          // create socket and connect
		ProgCommon.oos  = new java.io.ObjectOutputStream(sock.getOutputStream());  
		ProgCommon.ois  = new java.io.ObjectInputStream(sock.getInputStream());
	}

	/**
	 * Here, we close the sockets and reset static variables to guarantee a fresh next running
	 * @throws Exception
	 */
	private void cleanup() throws Exception {
		// close everything
		if (ProgCommon.oos != null) { 
			ProgCommon.oos.close();                                                   
		}

		if (ProgCommon.ois != null) {
			ProgCommon.ois.close();
		}

		if (sock != null) {
			sock.close();
		}



		// !! BUG FIX: K must be set to ZERO every Server running otherwise the decryption {@link YAOGC.E_AND_2_1 row 25} fails
		Wire.K = 0;

		System.out.println("Cleaned Everything");
	}

	protected void createCircuits() throws Exception {
		Circuit.isForGarbling = false;
		Circuit.setIOStream(ProgCommon.ois, ProgCommon.oos);
		for (int i = 0; i < ProgCommon.ccs.length; i++) {
			ProgCommon.ccs[i].build();
		}

		StopWatch.taskTimeStamp("circuit preparation");
	}

	protected void initializeOT() throws Exception {
		ProgCommon.oos.writeInt(otNumOfPairs);
		ProgCommon.oos.flush();

		rcver = new OTExtReceiver(otNumOfPairs, ProgCommon.ois, ProgCommon.oos);
		StopWatch.taskTimeStamp("OT preparation");
	}
}