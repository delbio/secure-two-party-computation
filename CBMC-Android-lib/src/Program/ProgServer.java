// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Program;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.io.output.CountingOutputStream;

import OT.OTExtSender;
import OT.Sender;
import Utils.StopWatch;
import YaoGC.Circuit;
import YaoGC.Wire;

public abstract class ProgServer extends Program {
	
    final private  int         serverPort   = 23456;             // server port number
    private ServerSocket       sock         = null;              // original server socket
    private Socket             clientSocket = null;              // socket created by accept

    protected Sender snder;
    protected int otNumOfPairs;
    protected int otMsgBitLength = Wire.labelBitLength;
    
    public void run() throws Exception {
	create_socket_and_listen();

	super.run();

	cleanup();
    }

    protected void init() throws Exception {
	Program.iterCount = ProgCommon.ois.readInt();


	super.init();
    }

    private void create_socket_and_listen() throws Exception {

	sock = new ServerSocket(serverPort);            // create socket and bind to port
	System.out.println("Waiting for client to connect");
	if (sock != null) clientSocket = sock.accept();                   // wait for client to connect
	
	
	System.out.println("Client is connected");
	
	
	CountingOutputStream cos = new CountingOutputStream(clientSocket.getOutputStream());
	CountingInputStream  cis = new CountingInputStream(clientSocket.getInputStream());
	
	
	
	ProgCommon.oos = new ObjectOutputStream(cos);
	ProgCommon.ois = new ObjectInputStream(cis);
	
	StopWatch.cos = cos;
	StopWatch.cis = cis;
    }

    
    /**
     * Here, we close the sockets and reset static variables to guarantee a fresh next running
     * @throws Exception
     */
    public void cleanup() throws Exception {
    
	if (ProgCommon.oos != null) ProgCommon.oos.close();	// close everything
	if (ProgCommon.ois != null) ProgCommon.ois.close();	// close everything
	
	
	if (clientSocket != null) clientSocket.close();
	if (sock != null) sock.close();
	
	ProgCommon.oos = null;
	ProgCommon.ois = null;
	
	// !! BUG FIX: K must be set to ZERO every Server running otherwise the decryption {@link YAOGC.E_AND_2_1 row 25} fails
	Wire.K = 0;
	
	
	System.out.println("Cleaned Everything");
    }

    protected void initializeOT() throws Exception {
	otNumOfPairs = ProgCommon.ois.readInt();

	snder = new OTExtSender(otNumOfPairs, otMsgBitLength, ProgCommon.ois, ProgCommon.oos);
	StopWatch.taskTimeStamp("OT preparation");
    }

    protected void createCircuits() throws Exception {
	Circuit.isForGarbling = true;
	Circuit.setIOStream(ProgCommon.ois, ProgCommon.oos);
	for (int i = 0; i < ProgCommon.ccs.length; i++) {
	    ProgCommon.ccs[i].build();
	}
	
	StopWatch.taskTimeStamp("circuit preparation");
    }
}