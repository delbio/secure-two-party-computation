// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Program;

import java.math.*;

import YaoGC.*;
import Utils.*;

public class CBMCProgramClient extends ProgClient {
	
    private BigInteger cBits;
	private BigInteger[] serverInputKeys, clientInputKeys;
    private State outputState;

    public CBMCProgramClient(BigInteger bv, int serverLen, int clientLen) {
    	CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH = serverLen;
    	CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH = clientLen;
    	cBits = bv;
    }

    protected void init() throws Exception {
//    	int bitVecLen = CBMCCircuitCommon.ois.readInt();
//    	System.out.println("bitVecLen:" + bitVecLen);
    	CBMCCircuitCommon.initCircuits();

    	otNumOfPairs = CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH;

    	super.init();
    }

    protected void execTransfer() throws Exception {
    	serverInputKeys = new BigInteger[CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH];

		for (int i = 0; i < CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH; i++) {
		    int bytelength = (Wire.labelBitLength-1)/8 + 1;
		    serverInputKeys[i]   = Utils.readBigInteger(bytelength, CBMCCircuitCommon.ois);
		}
		StopWatch.taskTimeStamp("receiving labels for peer's inputs");
	
		clientInputKeys = new BigInteger[CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH];
		rcver.execProtocol(cBits);
		clientInputKeys = rcver.getData();
		StopWatch.taskTimeStamp("receiving labels for self's inputs");
    }

    protected void execCircuit() throws Exception {
    	outputState = CBMCCircuitCommon.execCircuit(serverInputKeys, clientInputKeys);
    }


    protected void interpretResult() throws Exception {
	CBMCCircuitCommon.oos.writeObject(outputState.toLabels());
	CBMCCircuitCommon.oos.flush();
    }

    protected void verify_result() throws Exception {
	CBMCCircuitCommon.oos.writeObject(cBits);
	CBMCCircuitCommon.oos.flush();
    }
}