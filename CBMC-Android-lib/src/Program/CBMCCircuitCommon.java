// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Scanner;

import android.os.Environment;

import Utils.StopWatch;
import YaoGC.Circuit;
import YaoGC.State;
import YaoGC.myComponents.CBMCCircuit;

public class CBMCCircuitCommon extends ProgCommon {
	
	public static File confDir = new File (Environment.getExternalStorageDirectory(), "CBMC-Conf");
	
	public final static String OUTPUT_BITS_FILENAME = new File(confDir,"result.bits").getAbsolutePath();

	public static int countInputs(String filename) {
		int sum = 0;
		
		try {
			File inputsList = new File(filename);
			Scanner scanner = new Scanner(new FileReader(inputsList));
			while (scanner.hasNextLine()) {

				String line = scanner.nextLine();

				String[] tokens = line.split(" ");

				if (!tokens[0].contains("INPUT_") || (!(tokens.length == 3)))
					throw new Exception("Error on reading input file");

				sum+= Integer.parseInt(tokens[2]);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(66);
		}
		
		return sum;
	}
	
	
	
	public static HashMap<String, int[]> readCBMCInputMapping(String filename) {

		HashMap<String, int[]> inputPositions = new HashMap<String, int[]>();

		try {
//			File inputsList = new File(dir, filename);
			File inputsList = new File( filename);
			Scanner scanner = new Scanner(new FileReader(inputsList));
			while (scanner.hasNextLine()) {

				String line = scanner.nextLine();

				String[] tokens = line.split(" ");

				if (!tokens[0].contains("INPUT_") || (!(tokens.length == 3)))
					throw new Exception("Error on reading input file");

				int[] positionAndLength = new int[2];
				positionAndLength[0] = Integer.parseInt(tokens[1]);
				positionAndLength[1] = Integer.parseInt(tokens[2]);
				inputPositions.put(tokens[0], positionAndLength);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(66);
		}

		return inputPositions;
	}

	/*
	 * reads the file inputs.Server.txt
	 * 
	 * This file should contain the plain server inputs which will later be
	 * mapped to the circuit inputs.
	 * 
	 * return value can be an empty HashMap. In this case we will use standard values as input.
	 */
	public static HashMap<String, String> readInputValuesFromFile(String filename) {

		HashMap<String, String> inputPositions = new HashMap<String, String>();

		try {
			File inputsList = new File(filename);
			Scanner scanner = new Scanner(new FileReader(inputsList));
			while (scanner.hasNextLine()) {

				String line = scanner.nextLine();

				String[] tokens = line.split(" ");

				if (!tokens[0].contains("INPUT_") || (!(tokens.length == 2))) {
					System.out.println("Error on reading input file " + filename);
				}

				inputPositions.put(tokens[0], tokens[1]);
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + filename);
			return new HashMap<String, String>();
		}

		return inputPositions;
	}

	
	
	protected static void initCircuits() {
		ccs = new Circuit[1];
//		StopWatch.taskTimeStamp("Until here");
		File numberofgatesFile = new File(confDir, "output.numberofgates.txt");
		int gateCount = readIntFromFile(numberofgatesFile.getAbsolutePath());
		File noobFile = new File(confDir, "output.noob.txt");
		int outWires = readIntFromFile(noobFile.getAbsolutePath());
//		StopWatch.taskTimeStamp("Reading two ints");
		System.out.println("Number of Gates: " + gateCount);
			
		ccs[0] = new CBMCCircuit(gateCount, outWires);
	}
	
	private static int readIntFromFile(String fileName) {

		int out = 0;
		try {
			File numberOfGatesF = new File(fileName);
			Scanner scanner = new Scanner(new FileReader(numberOfGatesF));

			if (scanner.hasNextLine()) {
				String nOG = scanner.nextLine();
				out = Integer.parseInt(nOG);
			} else {
				throw new Exception("output.numberofgates.txt File empty");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(66);
		}
		return out;
	}

	public static State execCircuit(BigInteger[] slbs, BigInteger[] clbs) throws Exception {
		BigInteger[] lbs = new BigInteger[CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH + CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH];
		System.arraycopy(slbs, 0, lbs, 0, CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH);
		System.arraycopy(clbs, 0, lbs, CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH, CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH);
		State in = State.fromLabels(lbs);

		State out = ccs[0].startExecuting(in);

		StopWatch.taskTimeStamp("circuit garbling");

		return out;
	}
	
	public static class Constants {
		
		// Not really constants, not final, need to be read from file.
		public static int SERVER_INPUT_BITLENGTH;
		public static int CLIENT_INPUT_BITLENGTH;
		
		// final stuff
		public static final String FILE_CIRCUIT_GATES = new File(confDir,"output.gate.txt").getAbsolutePath();
		public static final String FILE_CONSTANT_WIRES = new File(confDir,"output.constants.txt").getAbsolutePath();
		public static final String FILE_INPUTS_SERVER = new File(confDir,"output.inputs.partyA.txt").getAbsolutePath();
		public static final String FILE_INPUTS_CLIENT = new File(confDir,"output.inputs.partyB.txt").getAbsolutePath();
		public static final String FILE_INPUT_WIRES = new File(confDir,"output.inputs.txt").getAbsolutePath();
		public static final String FILE_OUTDEGREE = new File(confDir,"output.noob.txt").getAbsolutePath();
		public static final String FILE_NUMBER_OF_GATES = new File(confDir,"output.numberofgates.txt").getAbsolutePath();
		
	}
}