// Copyright (C) 2010 by Yan Huang <yhuang@virginia.edu>

package Program;

import java.io.PrintWriter;
import java.math.BigInteger;

import Utils.StopWatch;
import Utils.Utils;
import YaoGC.State;
import YaoGC.Wire;

public class CBMCProgramServer extends ProgServer {

	private BigInteger sBits;

	private State outputState;

	private BigInteger[][] serverInputKeys, clientInputKeys;

	public CBMCProgramServer(BigInteger bv, int serverInputLength, int clientInputLength) {
		CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH = serverInputLength;
		CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH = clientInputLength;
		sBits = bv;
	}

	protected void init() throws Exception {
		CBMCCircuitCommon.initCircuits();

		generateLabelPairs();

		super.init();
	}

	private void generateLabelPairs() {
		serverInputKeys = new BigInteger[CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH][2];
		clientInputKeys = new BigInteger[CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH][2];

		for (int i = 0; i < serverInputKeys.length; i++) {
			serverInputKeys[i] = Wire.newLabelPair();
		}

		for (int i = 0; i < clientInputKeys.length; i++) {
			clientInputKeys[i] = Wire.newLabelPair();
		}
	}

	protected void execTransfer() throws Exception {
		for (int i = 0; i < CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH; i++) {
			int idx = sBits.testBit(i) ? 1 : 0;

			int bytelength = (Wire.labelBitLength-1)/8 + 1;
			Utils.writeBigInteger(serverInputKeys[i][idx], bytelength, CBMCCircuitCommon.oos);
		}
		CBMCCircuitCommon.oos.flush();
		StopWatch.taskTimeStamp("sending labels for selfs inputs");

				snder.execProtocol(clientInputKeys);
				StopWatch.taskTimeStamp("sending labels for peers inputs");
	}

	protected void execCircuit() throws Exception {
		BigInteger[] sBitslbs = new BigInteger[serverInputKeys.length];
		BigInteger[] cBitslbs = new BigInteger[clientInputKeys.length];

		for (int i = 0; i < serverInputKeys.length; i++)
			sBitslbs[i] = serverInputKeys[i][0];

		for (int i = 0; i < clientInputKeys.length; i++)
			cBitslbs[i] = clientInputKeys[i][0];

		outputState = CBMCCircuitCommon.execCircuit(sBitslbs, cBitslbs);
	}

	protected void interpretResult() throws Exception {
		BigInteger[] outLabels = (BigInteger[]) CBMCCircuitCommon.ois.readObject();

		BigInteger output = BigInteger.ZERO;
		for (int i = 0; i < outLabels.length; i++) {
						
			if (outputState.wires[i].value != Wire.UNKNOWN_SIG) {
				if (outputState.wires[i].value == 1) 
					output = output.setBit(i);

				continue;
			}
			else if (outLabels[i].equals(outputState.wires[i].invd ? 
					outputState.wires[i].lbl :
						outputState.wires[i].lbl.xor(Wire.R.shiftLeft(1).setBit(0)))) {
				output = output.setBit(i);
			}
			else if (!outLabels[i].equals(outputState.wires[i].invd ? 
					outputState.wires[i].lbl.xor(Wire.R.shiftLeft(1).setBit(0)) :
						outputState.wires[i].lbl)) 
			{
				throw new Exception("Bad label encountered: i = " + i + "\t" +
						outLabels[i] + " != (" + 
						outputState.wires[i].lbl + ", " +
						outputState.wires[i].lbl.xor(Wire.R.shiftLeft(1).setBit(0)) + ")");
			}
		}
		printOutputBitRep(output, outLabels.length);
		StopWatch.taskTimeStamp("output labels received and interpreted");
	}

	private void printOutputBitRep(BigInteger output, int numOutBits) {
		// Generate a String-representation of the Output bits
		String s_outBits = "";

		for (int i = 0; i < numOutBits; i++) {
			if (output.testBit(i))
				s_outBits = "1" + s_outBits;
			else
				s_outBits = "0" + s_outBits;
		}
		
		System.out.println("Concatenated Output Bits: " + s_outBits);
		
		try {
			PrintWriter writer = new PrintWriter(CBMCCircuitCommon.OUTPUT_BITS_FILENAME, "UTF-8");
//			PrintWriter writer = new PrintWriter(CBMCCircuitCommon.OUTPUT_BITS_FILENAME, "UTF-8");
			writer.println(s_outBits);
			writer.close();

		} catch (Exception e) {
			System.err.println("Problem while writing output file.");
			e.printStackTrace();
			System.exit(66);
		}
	}

	protected void verify_result() throws Exception {
		BigInteger cBits = (BigInteger) CBMCCircuitCommon.ois.readObject();

		BigInteger res = sBits.add(cBits);

		System.out.println("output (verify): " + res);
	}
}