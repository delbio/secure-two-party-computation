package YaoGC;

public class GTTCounter {

	private static GTTCounter sCrimeLab;
	private int mCounter;
	
	private GTTCounter(){
		mCounter = 0;
	}
	
	public static GTTCounter get(){
		if(sCrimeLab == null){
			sCrimeLab =  new GTTCounter();
		}
		return sCrimeLab;
	}
	
	public void counterPP(){
		mCounter++;
	}
	
	public int getCounter(){
		return mCounter;
	}
	
}