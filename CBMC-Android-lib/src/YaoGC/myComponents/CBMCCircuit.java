package YaoGC.myComponents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import Program.CBMCCircuitCommon;
import YaoGC.AND_2_1;
import YaoGC.Circuit;
import YaoGC.CompositeCircuit;
import YaoGC.OR_2_1;
import YaoGC.OR_L_1;
import YaoGC.Wire;
import YaoGC.XOR_2_1;
import YaoGC.XOR_L_1;

public class CBMCCircuit extends CompositeCircuit {

	private Vector<int[]> fixedInternals; // Format(?): Wire = {target-gate, input-port, constant-value};
	private Vector<int[]> myOutWires; // Format: OutWire = {sourceOutPort, i, outWireID};

	/**
	 * 
	 * @param nOG
	 *            Number of Gates -> size of array
	 */
	public CBMCCircuit(int nOG, int outDegree) {
		super(CBMCCircuitCommon.Constants.SERVER_INPUT_BITLENGTH + CBMCCircuitCommon.Constants.CLIENT_INPUT_BITLENGTH, outDegree, nOG, "CBMC-Circuit");
		this.fixedInternals = new Vector<int[]>();
		this.myOutWires = new Vector<int[]>();
	}

	protected void createSubCircuits() throws Exception {
		// StopWatch.taskTimeStamp("bis hier");
		readGates();
		// StopWatch.taskTimeStamp("readGates");
		super.createSubCircuits();
	}

	@Override
	protected void connectWires() throws Exception {
		// StopWatch.taskTimeStamp("bis hier");
		intraGateConnections();
		inputWireConnections();
		constantInputs();
		// StopWatch.taskTimeStamp("make connections");
	}

	@Override
	protected void defineOutputWires() {
		
		// TODO we have to initialize output wires which are input wires !!!!

		for (Iterator<int[]> it = myOutWires.iterator(); it.hasNext();) {
			int[] outW = it.next();
			// OutWire = {sourceOutPort, i, outWireID};
			if (outW[0] != 0) {
				throw new RuntimeException("output ports are always 0");
			}
			//System.arraycopy(subCircuits[outW[1]].outputWires, outW[0], outputWires, outW[2], 1);
			System.arraycopy(subCircuits[outW[1]].outputWires, 0, outputWires, outW[2], 1);
		}
	}

	protected void fixInternalWires() {

		for (Iterator<int[]> it = fixedInternals.iterator(); it.hasNext();) {
			// Format(?) 0: target-gate, 1: input-port, 2: source-gate 
			int[] intWire = it.next();
			int index1 = intWire[0];
			
			if (index1 < 0) {
				System.err.println("ASSERTION VIOLATION!");
				throw new RuntimeException("index1: " + index1);
			}
			
			int index2 = intWire[1];
			Circuit circuit = subCircuits[index1];
			Wire internalWire = circuit.inputWires[index2];
			//Wire internalWire = subCircuits[intWire[0]].inputWires[intWire[1]];
			internalWire.fixWire(intWire[2]);
		}
	}

	private final void readGates() throws FileNotFoundException {

		try {
			int len = (int) (new File(CBMCCircuitCommon.Constants.FILE_CIRCUIT_GATES).length());
			FileInputStream fis = new FileInputStream(CBMCCircuitCommon.Constants.FILE_CIRCUIT_GATES);
			byte buf[] = new byte[len];
			fis.read(buf);
			fis.close();

			// TODO: diese Werte evtl zwischenspeiechern? Werden nochmal in
			// intraGateConnections() verwendet
			String all = new String(buf);
			String lines[] = all.split("\n");

			for (int i = 0; i < lines.length; i++) {

				String tokens[] = lines[i].split(" ");
				String gateOP = tokens[0];
				int inputsCount = Integer.parseInt(tokens[1]);

				if (gateOP.equals("AND")) {
					// if (inputsCount != 2) {
					// System.err.println("And not yet implemented for " +
					// inputsCount + " Inputs");
					// System.exit(66);
					// }
					subCircuits[i] = AND_2_1.newInstance();
				} else if (gateOP.equals("OR")) {
					if (inputsCount == 2) {
						subCircuits[i] = OR_2_1.newInstance();
					} else {
						subCircuits[i] = new OR_L_1(inputsCount);
					}
				} else if (gateOP.equals("NOT")) {
					subCircuits[i] = new XOR_2_1();
				} else if (gateOP.equals("XOR")) {
					if (inputsCount > 2) {
						subCircuits[i] = new XOR_L_1(inputsCount);
					} else {
						subCircuits[i] = new XOR_2_1();
					}
				} else {
					System.out.println("Invalid Operation in readGates()");
					System.exit(666);
				}
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	private void intraGateConnections() {

		String lines[] = readAllLinesFromFile(CBMCCircuitCommon.Constants.FILE_CIRCUIT_GATES);

		for (int i = 0; i < lines.length; i++) {

			String tokens[] = lines[i].split(" ");
			String gateOP = tokens[0];

			// verhindern das bei einem Not(x) (== XOR(x,1)) mehrfach fixedInternals geschrieben werden
			boolean notWireSet = false;

			// start with 2!
			// 0 contains gateOp
			// 1 contains number of Inputs
			for (int j = 2; j < tokens.length; j++) {

				// token contains outPort and Gatenumber and inPort:
				String[] wireData = tokens[j].split(":");

				int sourceOutPort = Integer.parseInt(wireData[0]);
				if (sourceOutPort != 0) {
					throw new RuntimeException("outgoing points have to be 0! (remove these ports from implementation)");
				}
				int gateID = Integer.parseInt(wireData[1]);
				int targetInPort = Integer.parseInt(wireData[2]);

				if (gateID < 0) {
					int outWireID = ((-1) * gateID ) - 1;
					int[] actOutWire = {sourceOutPort, i, outWireID};
					myOutWires.add(actOutWire);
					
					if (gateOP.equals("NOT")) {

						// NOT triggert XOR mit konstanter 1
						// Folgegatter wir daher oben schon richtig gesetzt. Nur noch 
						// XOR konstant setzen.
						
						// Konstantes wire muss nur einmal gesetzt werden:
						if (!notWireSet) {
							
							if (i < 0) {
								System.err.println("ASSERTION VIOLATION!");
								throw new RuntimeException("i: " + i);
							}
							
							int[] setFixedWire = { i, 1, 1 }; // immer Port 1 konstant 1, port 0 ist Eingangssignal vom NOT
							fixedInternals.add(setFixedWire);
							notWireSet = true;
						}
					} 
				} else {
					
					gateID--; // wir starten bei Gate 1 in der Datei, der Array aber bei 0

					subCircuits[i].outputWires[sourceOutPort].connectTo(subCircuits[gateID].inputWires, targetInPort);

					if (gateOP.equals("NOT")) {

						// NOT triggert XOR mit konstanter 1
						// Folgegatter wir daher oben schon richtig gesetzt. Nur noch 
						// XOR konstant setzen.
						
						// Konstantes wire muss nur einmal gesetzt werden:
						if (!notWireSet) {
							
							if (i < 0) {
								System.err.println("ASSERTION VIOLATION!");
								throw new RuntimeException("i: " + i);
							}
							
							int[] setFixedWire = { i, 1, 1 }; // immer Port 1 konstant 1, port 0 ist Eingangssignal vom NOT
							fixedInternals.add(setFixedWire);
							notWireSet = true;
						}
					} 
					
					// Security check, f��r Performance auskommentieren:
					//Auskommentiert f��r speedtest
//					if (!(gateOP.equals("AND") || gateOP.equals("NOT") || gateOP.equals("OR") || gateOP.equals("XOR") || gateOP.equals("OR") || gateOP.equals("MUX") || gateOP.equals("ADDER-NCO")  || gateOP.equals("ADDER-OCO") || gateOP.equals("ADDER"))) {
//						System.out.println("Op Invalid in intraGate connection");
//						System.exit(666);
//					}
				}
			}

		}
	}

	private void inputWireConnections() {

		String lines[] = readAllLinesFromFile(CBMCCircuitCommon.Constants.FILE_INPUT_WIRES);

		for (int i = 0; i < lines.length; i++) {
			
//			System.out.println("Process line: " + lines[i]);

			String tokens[] = lines[i].split(" ");
			String inWireString = tokens[0];

			String[] iWString = inWireString.split("#");

			if (!iWString[0].equals("InWire:")) {
				System.err.println("Syntax error inwires");
				System.exit(66);
			}

			int inWireID = Integer.parseInt(iWString[1]) - 1; // subtract 1,
																// make sure
																// that this is
																// correct

			
			/*if (tokens.length == 1) {
				System.out.println("Input wire " + inWireID + " is not connected to anything...");
				
				Circuit[] tmpCircuits = new Circuit[this.nSubCircuits + 1];
				
				for (int index = 0; index < this.nSubCircuits; index++) {
					tmpCircuits[index] = this.subCircuits[index];
				}
				
				//Circuit lCircuit = AND_2_1.newInstance();
				//try {
				//	lCircuit.build();
				//} catch (Exception e) {
				//	// TODO Auto-generated catch block
				//	e.printStackTrace();
				//	System.exit(-1);
				//}
				
				//tmpCircuits[this.nSubCircuits] = lCircuit;
				
				//inputWires[inWireID].connectTo(lCircuit.inputWires, 0);
				//inputWires[inWireID].connectTo(lCircuit.inputWires, 1);
				
				int[] setFixedWire = { this.nSubCircuits, 1, 1 }; // wir arbeiten
				// erstmal nur mit
				// const 1ern
				fixedInternals.add(setFixedWire);
				
				this.nSubCircuits++;
				
				this.subCircuits = tmpCircuits;
			}
			else {*/
				for (int j = 1; j < tokens.length; j++) {
	
					// next token contains Port and Gatenumber
					// Seperate port and Gatenumber:
					String portAndGate2[] = tokens[j].split(":");
	
	//				int outport = Integer.parseInt(portAndGate2[0]);
	//				if (outport != 0) {
	//					System.out.println("Irgendetwas stimmt hier nicht; Outport!=0");					
	//				}
					
					//int sourceOutPort = Integer.parseInt(portAndGate2[0]);
					int gateID = Integer.parseInt(portAndGate2[1]);
					int targetInPort = Integer.parseInt(portAndGate2[2]);
					
					//int gateID = Integer.parseInt(portAndGate2[1]);
					
					if (gateID < 0) {
						// input is directly connected to output
						// TODO is this correct ? 
						int outWireID = ((-1) * gateID ) - 1;
						//int[] actOutWire = {sourceOutPort, i, outWireID};
						//myOutWires.add(actOutWire);
						
						outputWires[outWireID] = inputWires[inWireID];
						
						
						//System.err.println("I'm not sure this is correct... (CBMCCircuit.java)");
						//throw new RuntimeException("check");
						
						/*
						
						
						
						Circuit[] tmpCircuits = new Circuit[this.nSubCircuits + 1];
						
						for (int index = 0; index < this.nSubCircuits; index++) {
							tmpCircuits[index] = this.subCircuits[index];
						}
						
						Circuit lCircuit = AND_2_1.newInstance();
						try {
							lCircuit.build();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.exit(-1);
						}
						
						tmpCircuits[this.nSubCircuits] = lCircuit;
						
						inputWires[inWireID].connectTo(lCircuit.inputWires, 0);
						
						lCircuit.outputWires[0].connectTo(outputWires, outWireID);
						
						int[] setFixedWire = { this.nSubCircuits, 1, 1 }; // wir arbeiten
						// erstmal nur mit
						// const 1ern
						fixedInternals.add(setFixedWire);
						
						this.nSubCircuits++;
						
						this.subCircuits = tmpCircuits;*/
						
					}
					else {
						gateID--; // wir starten bei Gate 1 in der Datei, der Array aber
						// bei 0
						inputWires[inWireID].connectTo(subCircuits[gateID].inputWires, targetInPort);
					}
				}
			//}
		}
	}

	private void constantInputs() {

		String lines[] = readAllLinesFromFile(CBMCCircuitCommon.Constants.FILE_CONSTANT_WIRES);
		if (lines == null)
			return; // emtpy file

		for (int i = 0; i < lines.length; i++) {

			String tokens[] = lines[i].split(" ");
			String constantToken = tokens[0];

			if (!constantToken.equals("ONE")) {
				throw new RuntimeException("Not implemented yet.");
			}

			for (int j = 1; j < tokens.length; j++) {
				// next token contains Port and Gatenumber
				// Seperate port and Gatenumber:
				String portAndGate2[] = tokens[j].split(":");

				int gateID = Integer.parseInt(portAndGate2[1]);
				
				if (gateID < 0) {
					int outWireID = ((-1) * gateID ) - 1;
					
					// initialize this output wire (?)
					outputWires[outWireID] = new Wire();
					outputWires[outWireID].fixWire(1); // set value to constant ONE
				}
				else {
					gateID--; 	// wir starten bei Gate 1 in der Datei, der Array aber
								// bei 0
					int port = Integer.parseInt(portAndGate2[2]);

					int[] setFixedWire = { gateID, port, 1 }; 	// wir arbeiten
																// erstmal nur mit
																// const 1ern
					fixedInternals.add(setFixedWire);
				}
			}
		}
	}

	private String[] readAllLinesFromFile(String fileName) {

		try {
			int len = (int) (new File(fileName).length());
			if (len == 0)
				return null;
			
			FileInputStream fis = new FileInputStream(fileName);
			byte buf[] = new byte[len];
			fis.read(buf);
			fis.close();

			String all = new String(buf);
			String lines[] = all.split("\n");

			return lines;
		} catch (IOException e) {
			System.err.println(e);
			System.exit(66);
		}
		return null;
	}
}
