package Utils;

import java.io.IOException;


/**
 * 
 * @author Gianpiero Costantino
 * 
 * This class is used by Client and Server to store all variables that are saved after the STC function and
 * that must be passed to the User Interface. In particular, the Server uses the method "evaulateServerOutput" to
 * evaluate the output of the computation. We decided to put this method in this library to avoid additional dependencies.
 * So a generic process can import just the CBMC-Android-lib to execute client and server party
 *
 */

public class commonOutput {

	//String used by the Client to store the output got bu the server
	public static String computationClientOutput = "";

	
	/**
	 * The Server uses this static method to evaluate the output of the computation that it will send to the client afterwards
	 * @return The output as string or an error
	 */	
	public static String evaulateServerOutput()
	{
		int[] output; // The array that contains the function output
		ProcessOutput getOutput = new ProcessOutput();

		String outputString = ""; //The Output in string
		try {
			output = getOutput.getComputatiOutput(); //we take the output
			for (int i=0; i < output.length; i++)
			{
				//We concatenate the output array in a string
				outputString = outputString.concat("output["+i+"]: "+output[0]+"\n");
				System.out.println("output["+i+"]: "+output[0]);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			outputString = e.getMessage();
		}
		
		return outputString;
	}

}
