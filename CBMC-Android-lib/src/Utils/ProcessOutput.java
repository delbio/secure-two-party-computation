package Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

import Program.CBMCCircuitCommon;

class VariableInfo {
	
	private String variable_name_;
	private Vector<Integer> map_;
	private boolean is_signed_;
	private int size_;
	
	public VariableInfo(String variable_name, boolean is_signed, int size) {
		variable_name_ = variable_name;
		is_signed_ = is_signed;
		size_ = size;
		map_ = new Vector<Integer>();
	}
	
	public String getVariableName() {
		return variable_name_;
	}
	
	public int getNumberOfBits() {
		return size_;
	}
	
	public boolean isSigned() {
		return is_signed_;
	}
	
	public void add(int index) {
		if (map_.size() >= getNumberOfBits()) {
			throw new IndexOutOfBoundsException();
		}
		map_.add(index);
	}
	
	public void add(String[] tokens, int offset) {
		add(tokens, offset, (tokens.length - offset));
	}
	
	public void add(String[] tokens, int offset, int length) {
		for (int i = offset; i < offset + length; i++) {
			int value = Integer.valueOf(tokens[i]).intValue() - 1;
			add(value);
		}
	}
	
	public int mapBit(int index) {
		if (map_.size() < getNumberOfBits()) {
			throw new RuntimeException("Mapping not finished!");
		}
		return map_.get(index);
	}
	
	public String toString() {
		String result = getVariableName() + " <";
		
		if (!isSigned()) {
			result = "U";
		}
		
		result += "INT";
		result += getNumberOfBits();
		result += ">: ";
		
		for (int i = 0; i < getNumberOfBits(); i++) {
			result += "[" + i + " -> " + mapBit(i) + "] ";
		}
		
		return result;
	}
	
}

class VariableInfos {
	
	class Entry {
		
		public Entry(VariableInfo info, int index) {
			info_ = info;
			index_ = index;
		}
		
		public VariableInfo info_;
		public int index_;
	}
	
	HashMap<Integer, Entry> inverse_mapping_;
	LinkedList<VariableInfo> variable_infos_;
	
	public VariableInfos() {
		inverse_mapping_ = new HashMap<Integer, Entry>();
		variable_infos_ = new LinkedList<VariableInfo>();
	}
	
	public void add(VariableInfo info) {
		for (int i = 0; i < info.getNumberOfBits(); i++) {
			Entry entry_ = new Entry(info, i);
			//System.out.println(info.mapBit(i) + " @ " + entry_.info_.getVariableName());
			inverse_mapping_.put(info.mapBit(i), entry_);
		}
		variable_infos_.add(info);
	}
	
	public Entry getEntry(int index) {
		return inverse_mapping_.get(index);
	}
	
	public LinkedList<VariableInfo> getVariableInfos() {
		return variable_infos_;
	}
	
	public void print_mapping() {
		for (Integer key : inverse_mapping_.keySet()) {
			System.out.print("... " + key + ": ");
			
			Entry entry_ = inverse_mapping_.get(key);
			
			System.out.println(entry_.info_.getVariableName());
		}
	}
	
}

class ProcessOutput {
	
  public static void process_mapping_file(VariableInfos variable_infos) {
    // file: output.mapping.txt
	  
    try {
    	
    	
    	
      Scanner scanner = new Scanner(new FileReader(new File(CBMCCircuitCommon.confDir,"output.mapping.txt")));
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        //System.out.println(line);
        
        String[] tokens = line.split(" ");
        
        if (tokens.length > 0) {
        	// tokens[0]: variable name
        	// tokens[1]: variable type
        	// tokens[2..(n+2)]: data bits 0 - n
        	
        	boolean is_signed = !(tokens[1].startsWith("U"));
        	int offset = (is_signed?0:1);
        	if (!tokens[1].substring(offset, offset + 3).equals("INT")) {
        		throw new RuntimeException();
        	}
        	
        	int size = Integer.valueOf(tokens[1].substring(offset + 3)).intValue();
        	
        	VariableInfo info = new VariableInfo(tokens[0], is_signed, size);
        	
        	info.add(tokens, 2); 

        	variable_infos.add(info);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(-1);
    }
  }

  public static ArrayList<Boolean> process_output_file() throws IOException {
    // file: result.bits
	  
	  FileReader reader = new FileReader(new File(CBMCCircuitCommon.confDir,"result.bits"));
	  
	  int value = reader.read();
	  
	  ArrayList<Boolean> result = new ArrayList<Boolean>();
	  
	  while (value != -1) {
		  if (value == '0') {
			  result.add(false);
		  }
		  else if (value == '1') {
			  result.add(true);
		  }
		  else {
			  //System.out.println("Blaaaa: " + (char)value);
			  //throw new RuntimeException();
		  }
		  
		  value = reader.read();
	  }
	  
	  return result;
  }

  /**
   * THis method is used at the end of the computation to get the function output	
   * @return
   * @throws IOException
   */
  public int[] getComputatiOutput() throws IOException {

	VariableInfos infos = new VariableInfos();
	  
    process_mapping_file(infos);

    HashMap<String, Integer> array_indizes = new HashMap<String, Integer>();
    int[] values = new int[infos.getVariableInfos().size()];
    
    for (VariableInfo info : infos.getVariableInfos()) {
    	int size = array_indizes.size();
    	array_indizes.put(info.getVariableName(), size);
    	values[size] = 0;
    }
    
    ArrayList<Boolean> result = process_output_file();
    
    for (VariableInfo info : infos.getVariableInfos()) {
    	
    	int array_index = array_indizes.get(info.getVariableName());
    	
    	System.out.print(info.getVariableName() + " ");
    	
    	for (int i = 0; i < info.getNumberOfBits(); i++) {
    		int mapped_index = info.mapBit(i);
    		
    		if (result.get(result.size() - mapped_index - 1)) {
    			values[array_index] |= (1 << i);
    		}
    	}
    	
    	System.out.println(values[array_index]);
    }
    
    //All output arrays is given back
    return values;
  }
  
  public static void main2(String[] args) throws IOException {

	  VariableInfos infos = new VariableInfos();

	  process_mapping_file(infos);

	  HashMap<String, Integer> array_indizes = new HashMap<String, Integer>();
	  int[] values = new int[infos.getVariableInfos().size()];

	  for (VariableInfo info : infos.getVariableInfos()) {
		  //System.out.println(info);
		  int size = array_indizes.size();
		  array_indizes.put(info.getVariableName(), size);
		  values[size] = 0;
	  }

	  ArrayList<Boolean> result = process_output_file();

	  int i = 1;

	  for (Boolean value : result) {
		  int index = result.size() - i;

		  //System.out.print(index + " @ ");

		  VariableInfos.Entry entry = infos.getEntry(index);

		  int array_index = array_indizes.get(entry.info_.getVariableName());

		  //System.out.println("entry.index_ = " + entry.index_);

		  if (value) {
			  values[array_index] |= (1 << entry.index_);
		  }

		  //System.out.println(entry.info_.getVariableName());

		  i++;
	  }

	  for (VariableInfo info : infos.getVariableInfos()) {
		  System.out.println(info.getVariableName() + " " + values[array_indizes.get(info.getVariableName())]);
	  }

	  //infos.print_mapping();
  }

}

