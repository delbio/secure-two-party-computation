package cbmc.android.server;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class CBMCServerService extends Service {

	private static final String TAG = "CBMCServerService";

	private static final String INTENT_PARENT_NAME = CBMCServerService.class.getCanonicalName() + ".";

	public static final String KEY_ABILITA_CBMC_SERVER = INTENT_PARENT_NAME+"abilita-cbmc-server";

	// INTENT ACTION

	public static final String START_CBMC_SERVER_TASK = INTENT_PARENT_NAME + "start-cbmc-task";
	public static final String STOP_CBMC_SERVER_TASK = INTENT_PARENT_NAME + "stop-cbmc-task";
	

	/***
	 * Fai eseguire un'azione al servizio CBMCServerService
	 * @param c contesto per recuperare risorse
	 * @param action azione da eseguire
	 */
	public static void exec ( Context c, String action ) {
		Intent i = new Intent( c.getApplicationContext(), CBMCServerService.class);
		i.setAction(action);

		c.getApplicationContext().startService(i);
	}

	private Thread mThreadCBMC;
	private CBMCServerTask runnableCBMC;

	@Override
	public IBinder onBind(Intent intent) { return null; }

	/***
	 * Operazioni da eseguire prima di della terminazione del service
	 */
	public void onDestroy(){

		terminateCBMCThread();

		super.onDestroy();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		try{

			if (intent == null){ // then leave it in the started state but don't retain this delivered intent
				if (BuildConfig.DEBUG){
					String message = String.format( "intent == null @ time: %s", getTime() );
					Log.i(TAG, message );
				}
				startTaskCBMC();

				return START_STICKY;
			}

			if (BuildConfig.DEBUG){
				String message = String.format( "intent.Action: %s @ time: %s", intent.getAction(), getTime() );
				Log.i(TAG, message );
			}

			if (intentHandlingCBMCServer(intent)){
				return START_STICKY;
			}

			// Se intent.getAction == null o qualsiasi altra azione non definita da questo servizio
			// eseguo la scansione del sistema
			startTaskCBMC();

		} catch (Exception e) {
			if (BuildConfig.DEBUG){
				String message = String.format("errore onStartCommand @ time %s : ", getTime());
				Log.e(TAG, message,e);
			}
		}

		return START_STICKY;
	}

	private String getTime(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(cal.getTime());
	}

	public static boolean isEnabled ( Context context ) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences ( context );
		return prefs.getBoolean ( KEY_ABILITA_CBMC_SERVER, false );
	}

	public static void serviceEnable ( Context context, boolean abilitato ) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences ( context );
		Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putBoolean ( KEY_ABILITA_CBMC_SERVER, abilitato );
		prefsEditor.commit();
		
	}

	// ---- INTENT HANDLING
	private boolean intentHandlingCBMCServer (Intent intent){

		if ( START_CBMC_SERVER_TASK.equals ( intent.getAction() ) ){
			startTaskCBMC();
		
			return true;
		}
		if ( STOP_CBMC_SERVER_TASK.equals ( intent.getAction() ) ){
			terminateCBMCThread();
			return true;
		}
		
		return false;
	}

	// http://stackoverflow.com/questions/10961714/how-to-properly-stop-the-thread-in-java#10961760
	private void startTaskCBMC (){
		if ( ( mThreadCBMC != null ) && mThreadCBMC.isAlive()){
			if (BuildConfig.DEBUG) Log.i(TAG, "CBMCTask gia' in esecuzione");
		} else {
			runnableCBMC = new CBMCServerTask ( getApplicationContext() );
			
			mThreadCBMC = new Thread( runnableCBMC );
			mThreadCBMC.start();
			
		}
	}

	private void terminateCBMCThread() {
		if ( mThreadCBMC != null ) {
			runnableCBMC.terminate();
			
			if (BuildConfig.DEBUG) Log.i(TAG, "CBMCTask successfully closed");
		}
	}
}
