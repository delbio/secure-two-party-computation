package cbmc.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class CBMCServerFragment extends Fragment {

	private static final String TAG = "CBMCServerFragment";

	public Button mStartServerButton;
	public TextView mStatusConnectionTextView;
	public TextView mServerIPTextView;
	public CheckBox mServerAsAServiceCheckBox;

	private boolean isServerRun = false;

	private CBMCServerSingleAsyncTask mServerTask;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (BuildConfig.DEBUG) {
			// informazioni sulla memoria allocata per la VM
			String memory = "totalMemory: "+Runtime.getRuntime().totalMemory() +"byte"
					+" maxMemory: "+Runtime.getRuntime().maxMemory() +"byte"
					+" freeMemory: "+Runtime.getRuntime().freeMemory() +"byte"; 

			Log.v("MEMORY STATUS", memory);
		}
		this.setRetainInstance(true);

		// We get the message broadcast form CBMCServerTask to know the results of the STC function
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				
				String outputComputation = "";
				String GarbledGatesEvaluated = "";
		        String GatesInCircuit = "";
		        String NonXORGatesEvaluated = "";

				if (intent.getExtras() != null)
				{
					outputComputation = intent.getExtras().getString("computation_Output");			
					GarbledGatesEvaluated = intent.getExtras().getString("Garbled_Gates_Evaluated");
					GatesInCircuit = intent.getExtras().getString("Gates_in_Circuit");
					NonXORGatesEvaluated = intent.getExtras().getString("Non-XOR_Gates_Evaluated");
					
				}

				String output = String.format("Computation Result: %s Garbled_Gates_Evaluated: %s \n Gates_in_Circuit: %s \n Non-XOR_Gates_Evaluated: %s \n",
						outputComputation,
						GarbledGatesEvaluated,
						GatesInCircuit,
						NonXORGatesEvaluated);
				
//				We update the label
				mStatusConnectionTextView.setText(output);
				
			}
		}, new IntentFilter(CBMCServerTask.OUTPUT_RECEIVED)); //CBMCServerTask.OUTPUT_RECEIVED is the broadcastmessage filter
		
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_cbmc_server, container, false);

		mStatusConnectionTextView = (TextView) v.findViewById(R.id.status_connection_TextView);

		mServerIPTextView = (TextView) v.findViewById(R.id.server_ip_TextView);

		mStartServerButton = (Button) v.findViewById(R.id.start_server_Button);
		setTextStartServerButton();

		mStartServerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (isServerRun) {
					stopServer();
				} else {
					startServer();
				}

			}
		});

		mServerAsAServiceCheckBox = (CheckBox) v.findViewById(R.id.run_as_service_CheckBox);
		mServerAsAServiceCheckBox.setChecked(true);

		return v;
	}

	private void startServer(){

		if ( mServerAsAServiceCheckBox.isChecked() ){
			// Abilita il servizio
			CBMCServerService.serviceEnable(getActivity(), true);
			Intent i = new Intent( getActivity().getApplicationContext(), CBMCServerService.class);
			getActivity().getApplicationContext().startService(i);

			mStatusConnectionTextView.setText("");

		} else {
			mServerTask = new CBMCServerSingleAsyncTask(this);
			mServerTask.execute();
		}

		isServerRun = true;
		setTextStartServerButton();
	}

	public void stopServer(){
		if ( mServerAsAServiceCheckBox.isChecked() ){
			CBMCServerService.serviceEnable(getActivity(), false);

			Intent i = new Intent( getActivity().getApplicationContext(), CBMCServerService.class);
			getActivity().getApplicationContext().stopService(i);

		}

		isServerRun = false;
		setTextStartServerButton();
	}

	private void setTextStartServerButton(){
		if (isServerRun){
			mStartServerButton.setText("Stop Server");	
		} else {
			mStartServerButton.setText("Start Server");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		setServerIP();
	}

	private void setServerIP(){
		mServerIPTextView.setText(String.format("Server IP: %s", getIpAddress()));
	}

	public String getIpAddress() {
		WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
		return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
	}




}
