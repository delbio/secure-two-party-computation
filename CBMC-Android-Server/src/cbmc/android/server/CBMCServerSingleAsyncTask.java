package cbmc.android.server;

import android.os.AsyncTask;
import android.util.Log;


public class CBMCServerSingleAsyncTask extends AsyncTask<Void, String, Void>{
	
	private static final String TAG = "CBMCServerSingleAsyncTask";

	private RunCBMCProgramServer serverExecutor;
	
	private CBMCServerFragment mFragment;

	public CBMCServerSingleAsyncTask(CBMCServerFragment fragment){
		this.mFragment = fragment;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		mFragment.mStartServerButton.setEnabled(false);
		mFragment.mServerAsAServiceCheckBox.setEnabled(false);
		mFragment.mStatusConnectionTextView.setText("Waiting for Client connections...");
		
	}
	
	@Override
	protected Void doInBackground(Void... params) {


		try{
			String[] args = {
					""
			};

			long startTime = System.currentTimeMillis();

			RunCBMCProgramServer serverExecutor;
			serverExecutor = new RunCBMCProgramServer();
			

			serverExecutor.main(args);

			long endTime = System.currentTimeMillis();
			long execTime = (endTime - startTime);

			Log.d(TAG, String.format("**** time %sms", execTime) );
			this.publishProgress(String.format("Run finished: %sms.", execTime));

		} catch ( Exception e){
			Log.e(TAG, "error - ",e);
			this.publishProgress("Errore: "+e.getMessage());
			Log.d(TAG, "Server clean-up is running.");
			serverExecutor.cleanup();
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(String... status) {
		mFragment.mStatusConnectionTextView.append(status[0]);
	}

	@Override
	protected void onPostExecute(Void result) {
		mFragment.stopServer();
		mFragment.mStartServerButton.setEnabled(true);
		mFragment.mServerAsAServiceCheckBox.setEnabled(true);
	}
	
}
