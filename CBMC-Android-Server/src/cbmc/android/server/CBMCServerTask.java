package cbmc.android.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import Program.ProgCommon;
import Program.ProgServer;
import Program.Program;

import Utils.commonOutput;
import YaoGC.Circuit;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class CBMCServerTask implements Runnable {

	private static final String TAG = "CBMCServerTask";

	//It is the filter for the mbroadcast message
	public static final String OUTPUT_RECEIVED = "cbmc.output.received";

	private Context mContext;
	private static final int SLEEP_TIME = 1000;
	private volatile boolean stop = false;

	public CBMCServerTask(Context c) { mContext = c.getApplicationContext(); }

	public void terminate(){
		stop = true;
		onExit();
	}

	private boolean terminaTask(){
		if ( ( !CBMCServerService.isEnabled ( mContext ) ) || stop ) {
			if (BuildConfig.DEBUG) Log.i(TAG, TAG+" femrato da: "+(stop?"stop":"serviceAbilitato"));
			return true;
		}
		return false;
	}

	private long nextSleepTime;

	/***
	 * Operazioni di inizializzazione del task a ciclo continuo
	 */
	private void init(){
		nextSleepTime = SLEEP_TIME;

	}

	/***
	 * Operazioni di pulizia a termine esecuzione del task
	 */
	private void onExit(){
		if (serverExecutor != null) {
			serverExecutor.cleanup();
		} else {
			Log.e(TAG, "serverExecutor is null");
		}
	}


	/***
	 * Algoritmo di esecuzione a ciclo continuo
	 */
	@Override
	public void run() {
		if ( terminaTask() ) { return; }
		init();
		while (!stop) {

			if ( terminaTask() ) { return; }

			try{

				nextSleepTime = execGlobalMonitor( mContext );

			}catch(Exception e){
				Log.e(TAG, "taskOperation Exception -",e);
				nextSleepTime = SLEEP_TIME;
			}

			try {
				if (BuildConfig.DEBUG){ Log.i ( TAG, String.format ( "sleep for %s ms...", nextSleepTime ) ); }
				Thread.sleep(nextSleepTime);
			} catch (InterruptedException e) {
				if (BuildConfig.DEBUG){ Log.e ( TAG, "errore durante lo sleep: ",e ); }
			}
		}

	}

	private RunCBMCProgramServer serverExecutor;

	/***
	 * Method run every cycle by this task
	 * @param context
	 * @return
	 */
	private long execGlobalMonitor( Context context ){
		long startTime = System.currentTimeMillis();

		serverExecutor = new RunCBMCProgramServer();

		try{
			String[] args = {
					""
			};

			serverExecutor.main(args);
			// We send to the fragment the information of the function execution
			sendOutputResultUsingBroadCast();

		} catch ( Exception e){
			Log.e(TAG, "error - ",e);
			Log.d(TAG, "Running server cleanUp ...");
			serverExecutor.cleanup();
		}

		long endTime = System.currentTimeMillis();
		long execTime = (endTime - startTime);
		long nextTime = SLEEP_TIME - (execTime%SLEEP_TIME);

		if (BuildConfig.DEBUG){
			Log.d(TAG, 
					String.format("**** time: %s ms next_loop: %s ms", 
							execTime, nextTime 
							)
					);
		}
		return nextTime;

	}//END GLOBAL MONITOR

	private void sendOutputResultUsingBroadCast() {

		Intent outputBroadcast = new Intent(CBMCServerTask.OUTPUT_RECEIVED);

		//Here we read the computaton output
		String outputString = commonOutput.evaulateServerOutput(); //The Output in string


		//		All variables are casted to string and sent to the fragment
		outputBroadcast.putExtra("computation_Output", outputString);
		outputBroadcast.putExtra("Garbled_Gates_Evaluated", ""+Circuit.gateCounter);
		outputBroadcast.putExtra("Gates_in_Circuit", ""+Circuit.gateCounterConstructor);
		outputBroadcast.putExtra("Non-XOR_Gates_Evaluated", ""+Circuit.gateCounterConstructorNonXOR);

		//		We send the broadcast message with a particular filter
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(outputBroadcast);

		try {
			// Here we clean all sockets and streams of the Secure two party connection
			// Now we open annother socket with the client to send just the result of the computation 
			// DO NOT ASK ME WHY BUT WITH THE PREVIOUS SOCKET, THE CLIENT RECEIVED SHIT ON THE STREAM
			// So by creating a new socket the result is properly sent. 
			serverExecutor.cleanup();
			
			// The new listening socket is open at the same port of before plus one. So we are sure to not use previous channel
			int         serverPort   = 23456 + 1;             // server port number
			ServerSocket       sock         = null;              // original server socket
			Socket             clientSocket = null;              // socket created by accept
			sock = new ServerSocket(serverPort);            // create socket and bind to port
			
			//This is the separetor to split the correct output from the stream SHIT
			String streamEnd = "<>END";
			
			System.out.println("Waiting for client to connect");
			// Waiting for the client
			if (sock != null) clientSocket = sock.accept();
			System.out.println("Client is connected to receive output");
			
			
			OutputStream mmOutStream = clientSocket.getOutputStream();
			Log.d(TAG,"Sending function output to the Client: "+outputString);
			
			outputString = outputString.concat(streamEnd);
			
			//We send the output of the computation to the client
			mmOutStream.write(outputString.getBytes("UTF-8"));
			mmOutStream.flush();
			
			
			//Finally, we close everything to avoid socket already opened problem
			if (mmOutStream != null) mmOutStream.close();	// close everything
			if (clientSocket != null) clientSocket.close();
			if (sock != null) sock.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//Now are cleaning to get correct results for further STC execution 
		Circuit.gateCounter = 0;
		Circuit.gateCounterConstructor = 0;
		Circuit.gateCounterConstructorNonXOR = 0;
	}
}
