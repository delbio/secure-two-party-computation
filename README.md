CBMC-GC for Android
=========
It is the ported version of CBMC-GC to Android. CBMC-GC is a compiler for C programs in the context of secure two-party computation (STC). CBMC-GC compiles a C program that specifies a secure computation into a circuit which can be read in by an STC platform, which then performs the secure computation between two parties A and B.

CBMC-GC for Android allows developers to run garbled circuits compiled with CBMC-GC between two mobile devices with Android OS, like smartphones or tablets. At this version we provide:

  - An APP to run the client.
  - An APP to run the server.
  - The library needed to execute the compiled functions.


Installation
--------------

Download the source code of client, server and interpreter, which we call lib, and import them into Eclipse. Both client and server need the interpreter to properly run. So, you need to import the interpreter source code as project library as part of client and server project. Now, you are ready to run both client and server APP.

Usage
--------------
> Step 0
- When you compile a C program using cbmc-gc, it generates the following files:
  ***output.constants.txt, output.gate.txt, output.inputs.partyA.txt, output.inputs.partyB.txt, output.inputs.txt, output.mapping.txt,output.noob.txt, output.numberofgates.txt***. You have to put these files into the SD-card of your client and server smartphone. Currently, the default directory to create is "CBMC-Conf" in the SD root. In the folder put all files compiled by the CBMC-GC compiler plus the txt file for server input and client input.
  
> Step 1
- Run the Server APP and click on "Start Server". If you want to start teh server as service you have to tick the box (by default is ticked), otherwirse the server is started as task. The main difference is that when you run as service the server always listens to incoming conenction, even if the App in in background. Otherwise. the server waits for incoming connections only if the app is in foreground.


> Step 2
- Run the client APP and put in the box the IP address of the server. Then, click on connect and that's all.

Version
----

- 0.3 - Both Server and Client display the output of the STC function.

- 0.2.1 - Fixed a bug when run STC more than one time both Client and Server.

- 0.2 - Improved Client and Server User Interface.

- 0.1 - Deploying of Server and Client APPs for Android.

To Do
----
- Bug testing.

Team
-----------

These people work for CBMC-GC for Android Smartphone:

* [Gianpiero Costantino] - Post-Doc Researcher at IIT-CNR - [gianpiero.costantino@iit.cnr.it]
* Fabio Del Bene - Android Developer at IIT-CNR - [fabio.delbene@iit.cnr.it]
* [Fabio Martinelli] - Boss of the Security Groupt at IIT-CNR - [fabio.martinelli@iit.cnr.it]


External References
-----------

* [CBMC-GC] - CBMC-GC Webpage
* [CBMC-GC Manual] - CBMC-GC Manual

Other info
----------------
- To set up the server IP in code, go to LaunchFragment.java of the client project and change to server IP:
```JAVA 
String[] args = {
					"--server",
					"10.10.10.10"
				};
```


License
----

You can use and modify this software but please cite us and our work. Thanks!


[Gianpiero Costantino]:http://www.iit.cnr.it/staff/gianpiero.costantino/
[Fabio Martinelli]:http://wwwold.iit.cnr.it/staff/fabio.martinelli/
[CBMC-GC]:http://forsyte.at/software/cbmc-gc/
[CBMC-GC Manual]:http://forsyte.at/wp-content/uploads/cbmc-gc-v0.9.3.pdf

[gianpiero.costantino@iit.cnr.it]:mailto:gianpiero.costantino@iit.cnr.it
[fabio.delbene@iit.cnr.it]:mailto:fabio.delbene@iit.cnr.it
[fabio.martinelli@iit.cnr.it]:fabio.martinelli@iit.cnr.it